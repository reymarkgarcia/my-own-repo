import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientComponent } from '../app/components/client/client.component';
import { DashboardComponent } from '../app/components/dashboard/dashboard.component';
import { ClientDetailsComponent } from '../app/components/client-details/client-details.component';
import { LoginComponent } from '../app/components/login/login.component';
import { AuthenticationGuard } from '../../node_modules/microsoft-adal-angular6';
import { MsloginComponent } from '../app/components/mslogin/mslogin.component';
import { CallbackComponent } from '../app/components/login/callback.component';
import { AuthGuard } from '../app/components/login/auth.guard';
import { SettingsComponent } from '../app/components/settings/settings.component';

const routes: Routes = [
  { path: '', component: DashboardComponent, data:{routeName:"Dashboard"}, canActivate: [AuthGuard] },
  { path: 'mslogin', component: MsloginComponent, pathMatch:'full', canActivate: [AuthenticationGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'client', component: ClientComponent, canActivate: [AuthGuard], data:{routeName:"Clients"} },
  { path: 'client/:clientId', component: ClientDetailsComponent},
  { path: 'callback', component: CallbackComponent, canActivate: [AuthGuard] },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
