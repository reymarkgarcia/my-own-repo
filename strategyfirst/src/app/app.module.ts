import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ChartsModule } from '../../node_modules/ng2-charts';
import { Ng2TableModule } from '../../node_modules/ng2-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { JwPaginationComponent } from 'jw-angular-pagination';
import { HttpModule } from '@angular/http';
import { JwtModule } from "@auth0/angular-jwt";
import "isomorphic-fetch";
import { Client } from "@microsoft/microsoft-graph-client";
import { MsAdalAngular6Module, AuthenticationGuard } from '../../node_modules/microsoft-adal-angular6';

import {
  MatButtonModule,
  MatCardModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatGridListModule,
  MatProgressSpinnerModule,
  MatTableModule,
  MatSnackBarModule,
  MatSortModule,
  MatPaginatorModule,
  MatSelectModule,
  MatTabsModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientComponent } from './components/client/client.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ClientDetailsComponent } from './components/client-details/client-details.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from '../app/components/login/auth.guard';
import { MsloginComponent } from './components/mslogin/mslogin.component';
import { CallbackComponent } from './components/login/callback.component';
import { SettingsComponent } from './components/settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientComponent,
    DashboardComponent,
    JwPaginationComponent,
    ClientDetailsComponent,
    LoginComponent,
    MsloginComponent,
    CallbackComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ChartsModule,
    FormsModule,
    HttpModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSnackBarModule,
    MatSortModule,
    MatPaginatorModule,
    MatSelectModule,
    MatTabsModule,
    MsAdalAngular6Module,
    FormsModule,
    Ng2TableModule,
    NgbModule,
    NgxPaginationModule,
    HttpClientModule,
    MsAdalAngular6Module.forRoot({
      tenant: 'a15c7e3d-b0cc-4cb3-bd89-fbea6fe34f74',
      clientId: 'b81e8125-d79a-4f9c-bcf7-baa76f63a0fb',
      redirectUri: 'http://localhost:8000/callback/',
      endpoints: {
        "http://localhost:8000" : "/",
      },
      navigateToLoginRequestUrl: false,
      cacheLocation: 'localStorage',
    }),
  ],
  providers: [AuthenticationGuard],
  bootstrap: [AppComponent],
  entryComponents: [ClientComponent]
})
export class AppModule { }
