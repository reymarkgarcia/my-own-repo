import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Document } from '../../models/Document';
import { ClientService } from '../../components/client/client.service';
import { ClientDetailsService } from './client-details.service';
import { MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {
//   documentColumns: string[] = ['document_name'];
  displayedColumns: string[] = ['client_code', 'client_data', 'client_ref_code', 'document_heading'];
  documentsColumn: string[] = ['document_name'];
  clientId;
  documentId;
  documents:Document["data"];
  document;
  dataSource;

  constructor(private route: ActivatedRoute, private clientService:ClientService,
  private router:Router, private cDetailsService: ClientDetailsService) {
    this.clientId = this.route.snapshot.params['clientId'];
  }

  ngOnInit() {
    console.log(this.route);
    this.cDetailsService.getClientDocuments(this.clientId).subscribe(documents => {
    this.documents = <Document["data"]>(documents.data);
    console.log(documents);
    });
  }

  documentDetails(id){
    this.document = this.documents.filter(document => document.id === id);
    this.dataSource = new MatTableDataSource<Document>(this.document);
    console.log(this.dataSource);
  }


}
