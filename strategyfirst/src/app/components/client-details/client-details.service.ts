import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Document } from '../../models/Document';

@Injectable({
  providedIn: 'root'
})
export class ClientDetailsService {
documentByIdUrl:string = 'http://127.0.0.1:5000/docs/client';

    headerDict = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Authorization': 'Bearer ' + localStorage.getItem('refresh_token')
    }

    requestOptions = {
      headers: new HttpHeaders(this.headerDict)
    };

  constructor(private http:HttpClient) { }

  getClientDocuments(id: number):Observable<Document> {
    return this.http.get<Document>(`${this.documentByIdUrl}/${id}`, this.requestOptions);
  }
}
