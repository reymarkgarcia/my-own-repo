import { Component, OnInit, ViewChild } from '@angular/core';
import { Client } from '../../models/Client';
import { ClientService } from './client.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from '../../../../node_modules/rxjs';
import { PageEvent } from '@angular/material/paginator';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  clientItems:Client["data"];
  displayedColumns: string[] = ['entityid', 'clientName', 'agent', 'retainerAgreement', 'status', 'reports', 'createDate'];
  currentComponent;
  pageOfItems: Array<any>;

  constructor(private clientService:ClientService, private router:Router) {
      this.clientService.getClients().subscribe(clients => {
      this.clientItems = <Client["data"]>(clients.data);
      });
      console.log(localStorage.getItem("refresh_token"));
      console.log(this.clientItems);
  }

  ngOnInit() {
  }

  onChangePage(pageOfItems: Array<any>) {
        // update current page of items
        this.pageOfItems = pageOfItems;
  }

}
