import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Client } from '../../models/Client';
import { Document } from '../../models/Document';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  getClientsUrl:string = 'https://b654da66-eff1-44c9-a3af-b0dc39e4c232.mock.pstmn.io/clients/all';

  addClientUrl:string = 'http://127.0.0.1:8080/addClient';

  getDocumentsUrl:string = 'http://127.0.0.1:8080/getDocuments';

  constructor(private http:HttpClient) { }

  getClients():Observable<Client> {
    return this.http.get<Client>(this.getClientsUrl);
  }

  addClient(clientData):Observable<Client>{
    return this.http.post<Client>(this.addClientUrl,JSON.stringify(clientData), {
      headers: new HttpHeaders({
           'Content-Type':  'application/json',
         })
    });

  }

  getClientDocuments(id: number) {
//     return this.http.get<Document>(`${this.getDocumentsUrl}/${id}`);
    return 'docu sample'
  }

}
