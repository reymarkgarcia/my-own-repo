import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { MsAdalAngular6Service } from '../../../../node_modules/microsoft-adal-angular6';
import { Router } from '@angular/router';
import { JwtHelperService } from "../../../../node_modules/@auth0/angular-jwt";
import * as moment from '../../../../node_modules/moment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private adalSvc: MsAdalAngular6Service, private router: Router){
  }

  minutesLeft;
  minutesDifference;
  expDate;
  dateNow;
  jwtHelper = new JwtHelperService();

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.expDate = moment(this.jwtHelper.getTokenExpirationDate(this.adalSvc.accessToken));
    this.dateNow = moment(new Date());
    this.minutesDifference = moment.duration(this.expDate.diff(this.dateNow));
    this.minutesLeft = this.minutesDifference.asMinutes();

    if(!this.adalSvc.isAuthenticated){
      this.router.navigate(['/login']);
      return false;
    } else if(this.adalSvc.isAuthenticated && this.minutesLeft <=5){
      this.adalSvc.RenewToken('https://login.microsoftonline.com');
      return true;
    }
    return true;
  }

}
