import { Component, OnInit } from '@angular/core';
import { MsAdalAngular6Service } from '../../../../node_modules/microsoft-adal-angular6';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from '../../../../node_modules/rxjs';
import { GenericResponse } from '../../models/GenericResponse';
import { LoginService } from './login.service';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css']
})
export class CallbackComponent implements OnInit {

  dataSrc;
  genericResponse: GenericResponse;
  loginResponse;

  constructor(private adalSvc: MsAdalAngular6Service, private router: Router,
              private http: HttpClient, private loginService: LoginService) {
    this.getJSON().subscribe(data => {
    this.gatewayLogin(data);
    });
   }
  ngOnInit() {
  }

  public getJSON(): Observable<any> {
        return this.http.get("../../../assets/config/gateway-access.json");
  }

  gatewayLogin(user){
     this.loginService
      .gatewayLogin(user)
      .subscribe(response => {
      this.genericResponse = <GenericResponse>(response)
      console.log(response);
      localStorage.setItem('refresh_token', response.data.access_token);
      }, error => {
      });
      this.router.navigate(['/']);
  }

}
