import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MsAdalAngular6Service } from '../../../../node_modules/microsoft-adal-angular6';
import { Observable } from '../../../../node_modules/rxjs';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';
import { GatewayLoginResponse } from '../../models/GatewayLoginResponse';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  dataSrc;
  gatewayLoginResponse: GatewayLoginResponse;
  loginResponse;
  constructor(private router: Router, private adalSvc: MsAdalAngular6Service,
              private http: HttpClient, private loginService: LoginService) {

  }



  ngOnInit() {
  }

  login(){
    localStorage.clear();
    this.router.navigate(['/mslogin']);
  }

}
