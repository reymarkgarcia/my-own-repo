import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericResponse } from '../../models/GenericResponse';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loginUrl:string = 'http://127.0.0.1:5000/login';

  constructor(private http: HttpClient) { }

  gatewayLogin(loginData):Observable<GenericResponse>{
    return this.http.post<GenericResponse>(
            this.loginUrl,
            JSON.stringify(loginData),
            {
              headers: new HttpHeaders({
                'Content-Type':  'application/json',
              })
            }
            )

    ;

  }
}
