import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MsloginComponent } from './mslogin.component';

describe('MsloginComponent', () => {
  let component: MsloginComponent;
  let fixture: ComponentFixture<MsloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MsloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MsloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
