import { Component, OnInit } from '@angular/core';
import { MsAdalAngular6Service } from '../../../../node_modules/microsoft-adal-angular6';

@Component({
  selector: 'app-mslogin',
  templateUrl: './mslogin.component.html',
  styleUrls: ['./mslogin.component.css']
})
export class MsloginComponent implements OnInit {
  token;

  constructor(private adalSvc: MsAdalAngular6Service) {
    this.adalSvc.acquireToken('https://graph.microsoft.com')
    .subscribe((token: string) => {});
  }

  ngOnInit() {
  }

}
