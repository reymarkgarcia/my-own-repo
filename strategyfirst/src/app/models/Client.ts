export class Client{
    message: string;
    data: [{
      id:string;
      ClientName:string;
      Agent:string;
      RetainerAgreement:string;
      Status:string;
      CreateDate:String
    }];
    code:number;
}
