export class Document{
    message: string;
    data: [{
      client_code:string,
      client_data:string,
      client_ref_code:string,
      created_at:string,
      document_date:string,
      document_heading:string,
      filename:string,
      id:string,
      new_annual_amount:string,
      service_audit_type:string,
      service_provided_title:string,
      services_provided:string,
      text_file:string,
      updated_at:string
    }];
    code:number;
}
