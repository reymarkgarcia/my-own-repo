export interface GatewayLoginResponse {
  refresh_token: string;
  access_token: string;
}
