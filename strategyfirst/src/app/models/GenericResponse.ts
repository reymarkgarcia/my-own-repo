export interface GenericResponse {
  message: string;
  data: {
    refresh_token: string,
    access_token: string
  };
  code:number;
}
